const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const irc = require('irc');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static('public'));

let proxies = {}; // Array associativo de proxies

app.get('/', function(req, res) {
	let file;

  if (req.cookies.id)
    file = 'index.html';
  else
		file = 'login.html';

	res.sendFile(path.join(__dirname, file));
});

app.post('/login', function(req, res) {
	// Caso algum valor venha vazio, envia para a pagina de login novamente
	if (!(req.body.servidor && req.body.nome && req.body.canal)) {
		return res.redirect('/');
	}

	const crypto = require("crypto");
	let id;
	do {
		id = crypto.randomBytes(16).toString("hex"); // Gera uma string aleatoria que será o id
	} while (proxies[id]); // Enquanto existir o id, tenta novamente

	proxies[id] = {
		nick: req.body.nome,
		servidor: req.body.servidor,
		canais: [req.body.canal],
		socket: null,
		client: null // Será inicializado ao conectar o socket.io
	};

	res.cookie('id', id);
  res.redirect('/');
});

server.listen(process.env.PORT || 3000, function() {
  console.log('Server listening on port ' + (process.env.PORT || 3000) + '...');
});

// Funções de inicialização do Socket
const initializeCommands = require('./socket/commands');
const initializeEvents = require('./socket/events');
const initializeChannelEvents = require('./socket/channelEvents');

// Verifica se o ID fornecido é válido antes de abrir a conexão
io.use((socket, next) => {
  let id = socket.handshake.query.id;
	if (proxies.hasOwnProperty(id))
		next();
	else
		next(new Error('invalid id'));
});

io.on('connection', function(socket) {
	let id = socket.handshake.query.id;
	let proxy = proxies[id];
	proxy.socket = socket;

	socket.on('error', function(error) {
		console.log('[SOCKET ERROR] ', error);
	});

	proxy.close = function(message) {
		console.log('Deleting client ' + id + ' with message: ' + message);
		proxy.client.disconnect(message);
		delete proxies[id];
	}

	socket.on('disconnect', function() {
		// Se o cliente desconectar sem /quit, ele tem 30 segundos para reconectar antes de ser removido dos proxies
		if (!proxy.quit)
			proxy.timeout = setTimeout(proxy.close, 30000);
	});

	clearTimeout(proxy.timeout);

	if (!proxy.client) {
		proxy.client = new irc.Client(proxy.servidor, proxy.nick, {
			userName: proxy.nick,
  		realName: proxy.nick,
			channels: proxy.canais
		});
		proxy.canais = [];
		proxy.registered = false;

		// proxy.client.addListener('raw', function(message) {
		// 	console.log(message); // Apenas para debug
		// });

		// Inicializa o client com os listeners
		initializeEvents(proxy);
		initializeChannelEvents(proxy);
	}

	// Inicializa o Socket com os eventos
	initializeCommands(proxy);

	socket.emit('info inicial', {
		nick: proxy.nick,
		servidor: proxy.servidor,
		canais: proxy.canais,
		prefixCanal: proxy.client.opt.channelPrefixes,
		registered: proxy.registered
	});

	if (proxy.registered)
		proxy.client.list();
});

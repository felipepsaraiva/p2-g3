// Tratamento dos comandos enviados pelo usuario

module.exports = function(proxy) {
  let client = proxy.client;

  proxy.socket.on('nick', function(data) {
    client.send('nick', data);
  });

  proxy.socket.on('message', function(data) {
    client.say(data.to, data.message);
  });

  proxy.socket.on('notice', function(data) {
    client.notice(data.to, data.message);
  });

  proxy.socket.on('names', function(data) {
    client.send('names', data.channel);
  });

  proxy.socket.on('list', function() {
    client.list();
  });

  proxy.socket.on('join', function(channels) {
    client.join(channels);
  });

  proxy.socket.on('motd', function() {
    client.send("motd");
  });

  proxy.socket.on('whois', function(nick) {
    client.whois(nick);
  });

  proxy.socket.on('mode', function(args) {
    client.send('mode', ...args);
  });

  proxy.socket.on('part', function(data) {
    client.part(data.canal, data.mensagem);
  });

  proxy.socket.on('quit', function(mensagem) {
    proxy.quit = true;
    proxy.close(mensagem);
    clearTimeout(proxy.timeout);
  });

  proxy.socket.on('action', function (data) {
    client.action(data.target, data.message);
  });
}

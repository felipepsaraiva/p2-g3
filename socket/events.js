// Tratamento dos eventos gerais do cliente IRC

module.exports = function(proxy) {
  let client = proxy.client;

  client.addListener('error', function(message) {
    proxy.socket.emit('irc error', {
      text: message.args[message.args.length - 1]
    });
  });

  client.addListener('registered', function(message) {
    proxy.registered = true;
    proxy.socket.emit('registered');
    client.list();
  });

  client.addListener('nick', function(oldnick, newnick, channels, message) {
    proxy.socket.emit('nick', {
      oldnick: oldnick,
      newnick: newnick,
      channels: channels
    });
  });

  client.addListener('pm', function(nick, text, message) {
    proxy.socket.emit('pm', {
      from: nick,
      text: text
    });
  });

  client.addListener('notice', function(nick, to, text, message) {
    if (!nick)
      nick = 'Server';

    proxy.socket.emit('notice', {
      from: nick,
      to: to,
      text: text
    });
  });

  client.addListener('channellist_start', function() {
    proxy.socket.emit('channellist_start');
  });

  client.addListener('channellist_item', function(channel) {
    proxy.socket.emit('channellist_item',  channel);
  });

  client.addListener('motd', function(motd) {
    proxy.socket.emit('motd',motd);
  });

  client.addListener('whois', function(info) {
    proxy.socket.emit('whois', info);
  });

  client.addListener('+mode', function(channel, by, mode, argument, message) {
    proxy.socket.emit('mode', {
      channel: channel,
      by: by,
      mode: '+' + mode,
      argument: argument
    });
  });

  client.addListener('-mode', function(channel, by, mode, argument, message) {
    proxy.socket.emit('mode', {
      channel: channel,
      by: by,
      mode: '-' + mode,
      argument: argument
    });
  });

  client.addListener('quit', function(nick, reason, channels, message) {
    proxy.socket.emit('quit', {
      nick: nick,
      reason: reason,
      channels: channels
    });
  });

  client.addListener('kill', function (nick, reason, channels, message) {
    proxy.socket.emit('kill', {
      nick: nick,
      reason: reason,
      channels: channels,
    });
  });
}

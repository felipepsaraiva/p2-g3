// Tratamento dos eventos que o cliente pode receber

function initSocket(socket) {
	socket.on('disconnect', function() {
		$('button, #mensagem').prop('disabled', true);
		mudarTab(null, $('.canais-nav li').first());
    $('.canais-nav li[canal]').remove();

		var texto = '<div class="alert alert-danger conexao shadow msg-global center-block">'
			+ '<p>Houve um erro e <strong>perdemos a conexão</strong>, mas já estamos <strong>reconectando</strong>...</p>'
			+ '</div>';
    adicionarTexto(texto);
  });

	socket.on('error', function(error) {
		if (error == 'invalid id') {
			Cookies.remove('id');
			window.location = "/";
		}
	});

  socket.on('registered', function() {
    globals.registered = true;
		$('.conexao').fadeOut(function() {
			$('.conexao').remove();
		});
		$('button, #mensagem').prop('disabled', false);
  });

  socket.on('info inicial', function(info) {
    //console.log(info);
    globals.nick = info.nick;
    globals.servidor = info.servidor;
    globals.canais = info.canais;
		globals.prefixCanal = info.prefixCanal;
    globals.registered = info.registered;

    if (info.registered) {
			$('.conexao').fadeOut(function() {
				$('.conexao').remove();
			});
			$('button, #mensagem').prop('disabled', false);
		}

    $('#nickname').text(info.nick);

    var canais = info.canais;
    for (var i = 0; i < canais.length; i++) {
      adicionarTab(canais[i], false);
			let cmd = '/names ' + canais[i];
			comandos.names(socket, cmd.split(' '));
		}
  });

	socket.on('irc error', function(data) {
		adicionarErro(data.text, $('.canais-nav .active').attr('canal') || 'inicio');
	});

	socket.on('nick', function(data) {
		if (data.oldnick === globals.nick) {
			globals.nick = data.newnick;
			$('#nickname').text(data.newnick);
		}

		var nameItem = $('.painel-names p[nome="' + data.oldnick + '"]');
		nameItem.attr('nome', data.newnick);
		var newText = nameItem.text().replace(data.oldnick, data.newnick);
		nameItem.text(newText);

		for (var i = 0 ; i < data.channels.length ; i++)
    	anunciar(data.oldnick + ' agora é conhecido como ' + data.newnick, data.channels[i]);
  });

  socket.on('pm', function(data) {
    adicionarMensagem(data.text, 'inicio', data.from, globals.nick);
  });

  socket.on('message', function(data) {
    adicionarMensagem(data.text, data.to, data.from);
  });

	socket.on('notice', function(data) {
		adicionarMensagem(data.text, (isCanal(data.to) ? data.to : 'inicio'), '[' + data.from + ']');
	});

  socket.on('join', function(data) {
    if (data.nick == globals.nick)
      adicionarTab(data.channel, true);

    anunciar(data.nick + ' entrou no canal', data.channel);
		adicionarNome(data.channel, data.nick, '', true);
  });

  socket.on('part', function(data) {
		if (data.nick == globals.nick) {
			removerTab(data.channel);
		} else {
			var str = data.nick + ' saiu do canal';
	    if (data.reason)
	      str += ' (<i>' + data.reason + '</i>)';

			anunciar(str, data.channel);
			$('.name-ch-' + data.channel.replace('#', '') + '[nome="' + data.nick + '"]').remove();
		}
  });

	socket.on('names', function(data) {
		var nicks = data.nicks;
		$('.name-ch-' + data.channel.replace('#', '')).remove();

		for (var i = 0 ; i < nicks.length ; i++)
			adicionarNome(data.channel, nicks[i], data.modifiers[nicks[i]], false);

		atualizarContexto();
	});

	socket.on('channellist_start', function() {
		$('.painel-canais p').remove();
	});

	socket.on('channellist_item', function(channel) {
		adicionarCanal(channel.name, channel.users);
	});

	socket.on('motd', function(data){
		adicionarMensagem('<p class="motd">' + data.replace(/\n/g, '<br>') + '</p>', 'inicio', '[Server]');
	});

	socket.on('whois', function(data) {
		var texto = "Who is " + data.nick + "<br>" +
			"User: " + data.user + "<br>" +
			"Host: " + data.host + "<br>" +
			"Realname: " + data.realname + "<br>" +
			"Channels: " + (data.channels || "None") + "<br>" +
			"Server: " + data.server + "<br>" +
			"Server info: " + data.serverinfo + "<br>" +
			"Operator: " + (data.operator || "not an IRC Operator");
		adicionarMensagem(texto, $('.canais-nav .active').attr('canal') || 'inicio', '[Server]');
	});

	socket.on('mode', function(data) {
		var text = (data.by || 'Server') + ' estabeleceu o modo ' + data.mode;
		if (data.channel && data.argument)
			text += ' ' + data.argument;

		anunciar(text, data.channel || 'inicio');
	});

	socket.on('quit', function(data) {
		var str = data.nick + ' saiu do canal';
		if (data.reason)
			str += ' (<i>' + data.reason + '</i>)';

		for (var i = 0 ; i < data.channels.length ; i++) {
			$('.name-ch-' + data.channels[i].replace('#', '') + '[nome="' + data.nick + '"]').remove();
			anunciar(str, data.channels[i]);
		}
	});

	socket.on('invite', function(data) {
		var str = data.from + ' entá te convidando para o canal ' + data.channel;
		anunciar(str, $('.canais-nav .active').attr('canal') || 'inicio');
	});

	socket.on('kick', function(data) {
		if(data.nick == globals.nick) {
			var str = data.by + " te removeu do canal " + data.channel;
			if (data.reason)
				str += " (<i>" + data.reason + "</i>)";

			anunciar(str, 'inicio');
			removerTab(data.channel);
		} else {
			var str = data.by + " removeu " + data.nick + " do canal";
			if (data.reason)
				str += " (<i>" + data.reason + "</i>)";

			anunciar(str, data.channel);
			$('.name-ch-' + data.channel.replace('#', '') + '[nome="' + data.nick + '"]').remove();
		}
	});

	socket.on('kill', function (data) {
		var str = data.nick + ' saiu do canal';
		if (data.reason)
			str += ' (<i>' + data.reason + '</i>)';

		for (var i = 0 ; i < data.channels.length ; i++) {
			$('.name-ch-' + data.channels[i].replace('#', '') + '[nome="' + data.nick + '"]').remove();
			anunciar(str, data.channels[i]);
		}
	});

	socket.on('topic', function (data) {
		if(data.nick)
			var str = data.nick + " alterou o topico para: <i>" + data.topic + "</i>";
		else
			var str = "Topico: <i>" + data.topic + "</i>";

		anunciar(str, data.channel);
	});

	socket.on('action', function(data) {
		var str = "<i>" + data.from + " " + data.text + "</i>";

		if(isCanal(data.to))
			anunciar(str, data.to);
		else
			anunciar(str, 'inicio');
	});
}
